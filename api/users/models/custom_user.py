from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    ADMIN = 'admin'
    PROFESSOR = 'professor'
    STUDENT = 'student'

    PROFILE_CHOICES = (
        (ADMIN, 'Admin'),
        (PROFESSOR, 'Professor'),
        (STUDENT, 'Student'),
    )

    email = models.EmailField(('email address'), unique=True)
    date_joined = models.DateTimeField(('date joined'), auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    username = models.CharField(blank=True, null=True, max_length=64)
    profile = models.CharField(
        blank=True,
        choices=PROFILE_CHOICES,
        null=True,
        max_length=15,
        default=ADMIN,
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    class Meta:
        db_table = 'user'
