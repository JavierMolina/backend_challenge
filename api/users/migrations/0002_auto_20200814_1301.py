# Generated by Django 3.1 on 2020-08-14 13:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='profile',
            field=models.CharField(blank=True, choices=[('admin', 'Admin'), ('professor', 'Professor'), ('student', 'Student')], default='admin', max_length=15, null=True),
        ),
    ]
