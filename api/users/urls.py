from rest_framework.routers import DefaultRouter
from django.urls import path, include

from users.views.user import (
    UserViewSet,
)

from .views.health_check import HealthCheck
from .views.signup import SignUpView, LogOutView


ROUTER = DefaultRouter()
ROUTER.register('user', UserViewSet, basename='users')

APP_PATH_NAME = 'users'

urlpatterns = [
    path('health-check/', HealthCheck.as_view()),
    path(f'{APP_PATH_NAME}/', include(ROUTER.urls)),
    path(f'{APP_PATH_NAME}/sign-up/', SignUpView.as_view(), name='sign_up'),
    path(f'{APP_PATH_NAME}/log-out/', LogOutView.as_view(), name='log_out'),
]
