from rest_framework import viewsets, mixins
from users.models import User
from users.serializers.user import (
    UserSerializer,
)


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    '''
    User resource
     - Check if user exist if send "email" param,
     - Get user data if send own param.
     - Get object user data by id if includes /{id}/ in url path
    '''
    serializer_class = UserSerializer
    http_method_names = ['get']

    def get_queryset(self):
        queryset = User.objects.all()
        email = self.request.query_params.get('email', None)
        own = self.request.query_params.get('own', None)
        if email:
            queryset = queryset.filter(email=email)
            return queryset
        if own:
            queryset = queryset.filter(id=self.request.user.id)
            return queryset
        return queryset
