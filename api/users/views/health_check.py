from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny


class HealthCheck(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        '''Check if service is up and running'''
        return Response({'health_status': '👌'})
