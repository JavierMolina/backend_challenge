from django.contrib.auth import logout
from django.db import transaction
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics, views, status
from rest_framework.response import Response
from users.models import User
from users.serializers.user import UserSerializer


class SignUpView(generics.CreateAPIView):
    '''Sign up a user, this view is open.'''
    permission_classes = (AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class LogOutView(views.APIView):
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self):
        '''Logout confirmation, there's no actions yet.'''
        logout(self.request)
        return Response(status=status.HTTP_204_NO_CONTENT)
