from django.db import models

from .questions import Question


class Answer(models.Model):
    text = models.TextField(blank=False, null=False)
    question = models.ForeignKey(
        Question,
        related_name='answers',
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    its_correct = models.BooleanField(blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.text}'
