from django.db import models


class Question(models.Model):
    BOOLEAN = 'BOOLEAN'
    MULTIPLE_CHOICE_ONLY_ONE = 'MULTIPLE_CHOICE_ONLY_ONE'
    MULTIPLE_CHOICE_MORE_THAN_ONE = 'MULTIPLE_CHOICE_MORE_THAN_ONE'
    MULTIPLE_CHOICE_ALL_ANSWERED = 'MULTIPLE_CHOICE_ALL_ANSWERED'

    QUESTION_TYPE_CHOICES = (
        (BOOLEAN, 'boolean'),
        (MULTIPLE_CHOICE_ONLY_ONE, 'multiple_choice_only_one'),
        (MULTIPLE_CHOICE_MORE_THAN_ONE, 'multiple_choice_more_than_one'),
        (MULTIPLE_CHOICE_ALL_ANSWERED, 'multiple_choice_all_answered'),
    )

    text = models.CharField(
        max_length=128,
        blank=False,
        null=False,
    )
    question_type = models.CharField(
        max_length=32,
        blank=False,
        null=False,
        choices=QUESTION_TYPE_CHOICES
    )
    score = models.DecimalField(max_digits=4, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.question_type}'
