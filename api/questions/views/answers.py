from rest_framework import viewsets

from ..models.answers import Answer
from ..serializers.answers import AnswerSerializer


class AnswerModelViewset(viewsets.ModelViewSet):
    """Answer Crud"""
    queryset = Answer.objects.all()
    # http_method_names = ['get']
    serializer_class = AnswerSerializer
