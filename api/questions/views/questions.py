from rest_framework import viewsets

from ..models.questions import Question
from ..serializers.questions import QuestionSerializer


class QuestionModelViewset(viewsets.ModelViewSet):
    """Question Crud"""
    queryset = Question.objects.all()
    # http_method_names = ['get']
    serializer_class = QuestionSerializer
