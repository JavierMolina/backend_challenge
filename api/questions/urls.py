from rest_framework.routers import DefaultRouter
from django.urls import path, include

from .views.answers import AnswerModelViewset
from .views.questions import QuestionModelViewset


APP_PATH_NAME = 'questions'


ROUTER = DefaultRouter()
ROUTER.register('question', QuestionModelViewset, basename='question')
ROUTER.register('answer', AnswerModelViewset, basename='answer')

urlpatterns = [
    path(f'{APP_PATH_NAME}/', include(ROUTER.urls)),
]
