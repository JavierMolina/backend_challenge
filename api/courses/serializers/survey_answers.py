from django.db import transaction
from rest_framework import serializers

from ..models.survey_answers import SurveyAnswer
from ..models.survey_options import SurveyOption
from .survey_options import SurveyAnswerOptionSerializer


class SurveyAnswerSerializer(serializers.ModelSerializer):
    options = SurveyAnswerOptionSerializer(many=True)

    class Meta:
        model = SurveyAnswer
        fields = [
            'id',
            'user',
            'survey',
            'question',
            'options',
            'score',
            'approved',
            'created',
            'modified',
        ]

    def _get_option_instances(self, options):
        return [
            SurveyOption.objects.create(
                answer=dict(item)['answer'],
                value=dict(item)['value']
            )
            for item
            in options
        ]

    def _add_options_to_survey_answer(self, option_instances, survey_answer):
        return [
            survey_answer.options.add(option)
            for option
            in option_instances
        ]

    @transaction.atomic
    def create(self, validated_data):
        options = validated_data.pop('options')
        survey_answer = SurveyAnswer.objects.create(**validated_data)
        option_instances = self._get_option_instances(options)
        self._add_options_to_survey_answer(option_instances, survey_answer)
        return survey_answer
