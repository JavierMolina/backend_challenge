from rest_framework import serializers

from ..models.user_courses import UserCourse
from users.models.custom_user import User


class UserCourseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'profile',
            'email',
        ]


class UserCourseSerializer(serializers.ModelSerializer):
    user = UserCourseUserSerializer()

    class Meta:
        model = UserCourse
        fields = [
            'course',
            'user',
            'approved',
            'created',
            'modified',
        ]
        depth = 2
