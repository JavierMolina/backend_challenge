from rest_framework import serializers

from ..models.survey_options import SurveyOption


class SurveyOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SurveyOption
        fields = [
            'id',
            'answer',
            'value',
            'created',
            'modified',
        ]


class SurveyAnswerOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SurveyOption
        fields = [
            'answer',
            'value',
        ]
