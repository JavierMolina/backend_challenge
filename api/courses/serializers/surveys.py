import itertools
from django.db.transaction import atomic
from rest_framework import serializers

from ..models.surveys import Survey


class SurveyDescriptionSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField('get_questions')
    answers = serializers.SerializerMethodField('get_answers')

    class Meta:
        model = Survey
        fields = [
            'id',
            'name',
            'user',
            'lesson',
            'questions',
            'answers',
            'minimum_passing_score',
            'created',
            'modified',
        ]

    def get_questions(self, survey):
        return survey.lesson.questions.values()

    def get_answers(self, survey):
        return list(itertools.chain.from_iterable([
            question.answers.select_related().values(
                'id',
                'question_id',
                'text',
            )
            for question
            in survey.lesson.questions.select_related()
        ]))


class SurveySerializer(serializers.ModelSerializer):

    class Meta:
        model = Survey
        fields = [
            'id',
            'name',
            'user',
            'lesson',
            'minimum_passing_score',
            'created',
            'modified',
        ]
