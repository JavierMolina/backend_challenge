from django.contrib import admin

from .models.courses import Course

admin.site.register(Course)
