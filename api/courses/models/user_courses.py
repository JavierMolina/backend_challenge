from django.db import models
from django.db.models import Avg

from users.models.custom_user import User

from .courses import Course


class UserCourse(models.Model):
    course = models.ForeignKey(
        Course,
        related_name='user_courses',
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    user = models.ForeignKey(
        User,
        related_name='user_courses',
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def _are_all_lessons_approved(self):
        return not [
            survey_answer.approved
            for survey_answer
            in self.user.survey_answers.select_related()
        ].count(False) > 0

    @property
    def approved(self):
        return self._are_all_lessons_approved()
