from django.db import models

from questions.models.answers import Answer


class SurveyOption(models.Model):
    answer = models.ForeignKey(
        Answer,
        related_name='survey_answers',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    value = models.BooleanField(null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
