from django.db import models
from django.db.models import Avg

from questions.models.questions import Question
from users.models.custom_user import User

from .lessons import Lesson


class Survey(models.Model):
    name = models.CharField(
        max_length=64,
        blank=False,
        null=False,
    )
    user = models.ForeignKey(
        User,
        related_name='surveys',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    lesson = models.ForeignKey(
        Lesson,
        related_name='surveys',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    minimum_passing_score = models.DecimalField(
        max_digits=4,
        decimal_places=2
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [
            models.Index(fields=['name']),
        ]

    def __str__(self):
        return f'{self.name}'
