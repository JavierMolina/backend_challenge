from django.db import models


class Course(models.Model):
    name = models.CharField(
        max_length=64,
        blank=False,
        null=False,
    )
    available = models.BooleanField(default=True, null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [
            models.Index(fields=['name']),
        ]

    def __str__(self):
        return f'{self.name}'
