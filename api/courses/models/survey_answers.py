from django.db import models

from questions.models.questions import Question
from questions.models.answers import Answer
from users.models.custom_user import User

from .surveys import Survey
from .survey_options import SurveyOption


class SurveyAnswer(models.Model):
    user = models.ForeignKey(
        User,
        related_name='survey_answers',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    survey = models.ForeignKey(
        Survey,
        related_name='survey_answers',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    question = models.ForeignKey(
        Question,
        related_name='survey_answers',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    options = models.ManyToManyField(
        SurveyOption,
        related_query_name='survey_answer',
        related_name='survey_answers',
        blank=False,
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def _get_answers_checked(self):
        return [
            survey_option.value == survey_option.answer.its_correct
            for survey_option
            in self.options.select_related()
        ]

    def _get_core_list(self, answers_checked, individual_answer_score):
        return [
            [0, 1][its_correct] * individual_answer_score
            for its_correct
            in answers_checked
        ]

    def _get_question_score(self, score_list):
        return sum(score_list)

    @property
    def score(self):
        answers_checked = self._get_answers_checked()
        answers_checked_length = len(answers_checked)

        if not answers_checked_length:
            return answers_checked_length

        individual_answer_score = self.question.score / answers_checked_length
        score_list = self._get_core_list(
            answers_checked,
            individual_answer_score
        )
        return self._get_question_score(score_list)

    @property
    def approved(self):
        return self.score >= self.survey.minimum_passing_score
