from django.db import models

from questions.models.questions import Question

from .courses import Course


class Lesson(models.Model):
    name = models.CharField(
        max_length=64,
        blank=False,
        null=False,
    )
    detail = models.CharField(
        max_length=64,
        blank=False,
        null=False,
    )
    course = models.ForeignKey(
        Course,
        related_name='lessons',
        on_delete=models.PROTECT,
        null=False,
        blank=False,
    )
    questions = models.ManyToManyField(
        Question,
        related_query_name='lesson',
        related_name='lessons',
        blank=False,
    )
    available = models.BooleanField(default=True, null=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [
            models.Index(fields=['name']),
        ]

    def __str__(self):
        return f'{self.name}'
