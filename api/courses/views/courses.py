from rest_framework import viewsets

from ..models.courses import Course
from ..serializers.courses import CourseSerializer


class CourseModelViewset(viewsets.ModelViewSet):
    """Course Crud"""
    queryset = Course.objects.all()
    # http_method_names = ['get']
    serializer_class = CourseSerializer
