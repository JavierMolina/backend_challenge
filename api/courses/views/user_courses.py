from rest_framework import viewsets

from ..models.user_courses import UserCourse
from ..serializers.user_courses import UserCourseSerializer


class UserCourseModelViewset(viewsets.ModelViewSet):
    """User courses Crud"""
    queryset = UserCourse.objects.all()
    serializer_class = UserCourseSerializer
