from rest_framework import viewsets

from ..models.survey_options import SurveyOption
from ..serializers.survey_options import SurveyOptionSerializer


class SurveyOptionModelViewset(viewsets.ModelViewSet):
    """Survey Option Crud"""
    queryset = SurveyOption.objects.all()
    serializer_class = SurveyOptionSerializer
