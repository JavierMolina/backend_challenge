from django.db import transaction
from rest_framework import viewsets
from rest_framework.response import Response

from users.models.custom_user import User

from ..models.lessons import Lesson
from ..serializers.lessons import LessonSerializer


class LessonModelViewset(viewsets.ModelViewSet):
    """Lesson Crud"""
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer

    @transaction.atomic
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if (self.request.user.profile == User.STUDENT):
            queryset = queryset.filter(available=True)
            import ipdb
            ipdb.set_trace()

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
