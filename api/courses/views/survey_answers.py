from rest_framework import viewsets

from ..models.survey_answers import SurveyAnswer
from ..serializers.survey_answers import SurveyAnswerSerializer


class SurveyAnswerModelViewset(viewsets.ModelViewSet):
    """Survey Answer Crud"""
    queryset = SurveyAnswer.objects.all()
    serializer_class = SurveyAnswerSerializer
