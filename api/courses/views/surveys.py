from rest_framework import viewsets

from ..models.surveys import Survey
from ..serializers.surveys import SurveySerializer, SurveyDescriptionSerializer


class SurveyModelViewset(viewsets.ModelViewSet):
    """Survey Crud"""
    queryset = Survey.objects.all()
    serializer_class = SurveySerializer


class SurveyDescriptorModelViewset(viewsets.ModelViewSet):
    """Survey Descriptor retrieve"""
    queryset = Survey.objects.all()
    http_method_names = ['get']
    serializer_class = SurveyDescriptionSerializer
