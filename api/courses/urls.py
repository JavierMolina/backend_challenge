from rest_framework.routers import DefaultRouter
from django.urls import path, include

from .views.courses import CourseModelViewset
from .views.lessons import LessonModelViewset
from .views.surveys import SurveyModelViewset, SurveyDescriptorModelViewset
from .views.survey_answers import SurveyAnswerModelViewset
from .views.survey_options import SurveyOptionModelViewset
from .views.user_courses import UserCourseModelViewset


APP_PATH_NAME = 'courses'


ROUTER = DefaultRouter()
ROUTER.register('course', CourseModelViewset, basename='course')
ROUTER.register('lesson', LessonModelViewset, basename='lesson')
ROUTER.register('survey', SurveyModelViewset, basename='survey')
ROUTER.register(
    'survey-descriptor',
    SurveyDescriptorModelViewset,
    basename='survey_descriptor'
)
ROUTER.register(
    'survey-answer',
    SurveyAnswerModelViewset,
    basename='survey_answer'
)
ROUTER.register(
    'survey-option',
    SurveyOptionModelViewset,
    basename='survey_option'
)
ROUTER.register(
    'user-course',
    UserCourseModelViewset,
    basename='user_course'
)

urlpatterns = [
    path(f'{APP_PATH_NAME}/', include(ROUTER.urls)),
]
