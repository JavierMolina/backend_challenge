#!/bin/bash
set -x

echo "Waiting for postgres..."

while ! nc -z api-db 5432; do
  echo 'sleep!'
  sleep 0.5
done

echo "PostgreSQL started"

python manage.py migrate
python manage.py loaddata --format=json dbSource.json

# Prepare log files and start outputting logs to stdout
touch ./logs/gunicorn.log
touch ./logs/gunicorn-access.log
tail -n 0 -f ./logs/gunicorn*.log &

echo "Start application"
exec gunicorn e_learning_courses.wsgi:application \
  --name e_learning_courses \
  --bind 0.0.0.0:8000 \
  --workers 4 \
  --log-level=error \
  --log-file=./logs/gunicorn.log \
  --access-logfile=./logs/gunicorn-access.log

"$@"
