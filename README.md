# e_learning_courses #

Api for online learning courses

## Technologic Stack ##

* [Anaconda](https://www.anaconda.com/)
* [Django](https://www.djangoproject.com/)
* [Django Rest Framework](https://www.django-rest-framework.org/)
* [Swagger](https://drf-yasg.readthedocs.io/en/stable/readme.html)
* [PostgreSQL](https://www.postgresql.org/)
* [Nginx](https://www.nginx.com/)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

## Setup Develop environment ##

* [Install Docker](https://docs.docker.com/install/)

* [Install Docker Compose](https://docs.docker.com/compose/install/)
  
* [Testing server in AWS](http://ec2-52-91-119-172.compute-1.amazonaws.com/api/v1/health-check/)

* Clone this repo

```bash
git clone git@gitlab.com:JavierMolina/backend_challenge.git
```

* Run services in container

```bash
docker-compose up --build -d
```

* To stop all cotainers

```bash
docker-compose stop
```

* To remove all cotainers

```bash
docker-compose down
```

### Run in develop mode ###

* Add *.env* file inside **/api** folder

```bash
# .env file
# Database connection
API_DATABASE_NAME='e_learning_courses'
API_DATABASE_USER='postgres'
API_DATABASE_PASSWORD='12345678'
API_DATABASE_HOST='0.0.0.0'
API_DATABASE_PORT='5434'
​
```

* Run database container

```bash
docker-compose up api-db -d
```

* Install python dependencies

```bash
cd /api
pip install requirements.txt
```

* Add static files

```bash
./manage.py collectstatic
```

* Create database schema

```bash
./manage.py migrate
```

* Load database default data

```bash
./manage.py loaddata --format=json dbSource.json
```

* Run develop server

```bash
./manage.py runserver
```

* Access to api documentation **localhost:8000/admin/**

* Login with default test user
  *admin*, *professor*, *student*
  password *12345678*

* Access to api documentation **localhost:8000/doc/**

* Use root path for any service *localhost:8000/api/v1/*
